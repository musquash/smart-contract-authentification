// SPDX-License-Identifier: MIT

/* Get this code from https://github.com/smartcontractkit/chainlink/blob/develop/contracts/src/v0.8/interfaces/OwnableInterface.sol*/

pragma solidity ^0.8.11;

interface OwnableInterface {
  function owner() external returns (address);

  function transferOwnership(address recipient) external;

  function acceptOwnership() external;
}