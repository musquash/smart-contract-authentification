// SPDX-License-Identifier: MIT

pragma solidity ^0.8.11;

import '../helper/ConfirmedOwner.sol';

contract BaseController is ConfirmedOwnerWithProposal {
    // The Contract Manager's address
    address managerAddr;

    constructor() ConfirmedOwnerWithProposal(msg.sender, address(0)) {}

    function setManagerAddress(address _managerAddr) public onlyOwner {
        managerAddr = _managerAddr;
    }
}
