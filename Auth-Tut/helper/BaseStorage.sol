// SPDX-License-Identifier: MIT

pragma solidity ^0.8.11;

import './ConfirmedOwner.sol';

contract BaseStorage is ConfirmedOwnerWithProposal {
    address controllerAddr;

    constructor() ConfirmedOwnerWithProposal(msg.sender, address(0)) {

    }

    modifier onlyController() {
        require(msg.sender == controllerAddr);
        _;
    }

    function setControllerAddr(address _controllerAddr) public onlyOwner {
        controllerAddr = _controllerAddr;
    }
}
