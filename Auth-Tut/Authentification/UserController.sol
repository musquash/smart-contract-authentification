// SPDX-License-Identifier: MIT

pragma solidity ^0.8.11;

import './ContractManager.sol';
import './UserStorage.sol';
import "../helper/BaseController.sol";

contract UserController is BaseController {

    function createUser(
        bytes32 _username,
        bytes32 _firstName,
        bytes32 _lastName,
        string memory _network,
        string memory _email,
        string memory _commentary
        ) public returns(uint) {
        ContractManager _manager = ContractManager(managerAddr);

        address _userStorageAddr = _manager.getAddress("UserStorage");
        UserStorage _storage = UserStorage(_userStorageAddr);

        require(_storage.addresses(msg.sender) == 0);
        require(_storage.usernames(_username) == 0);

        return _storage.createUser(
            msg.sender,
            _username,
            _firstName,
            _lastName,
            _network,
            _email,
            _commentary
            );
    }

}
