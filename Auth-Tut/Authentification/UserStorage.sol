// SPDX-License-Identifier: MIT

pragma solidity ^0.8.11;

import "../helper/BaseStorage.sol";
import "./UserController.sol";

contract UserStorage is BaseStorage {

    mapping(uint => Profile) public profiles;
    mapping (address => uint) public addresses;
    mapping (bytes32 => uint) public usernames;

    struct Profile {
        uint id;
        bytes32 username; // "If you can limit the length to a certain number of bytes, always use one of bytes1 to bytes32 because they are much cheaper."
        bytes32 firstName;
        bytes32 lastName;
        string bio;
        string email;
        string commentary;
    }

    uint public latestUserId = 0;
    address ownerAddr;
    // address controllerAddr;


    function createUser(
        address _address,
        bytes32 _username,
        bytes32 _firstName,
        bytes32 _lastName,
        string memory _bio,
        string memory _email,
        string memory _commentary
        ) public onlyController returns(uint) {

        latestUserId++;
        profiles[latestUserId] = Profile(
            latestUserId,
            _username,
            _firstName,
            _lastName,
            _bio,
            _email,
            _commentary
        );

        addresses[_address] = latestUserId;
        usernames[_username] = latestUserId;

        return latestUserId;
    }


}
